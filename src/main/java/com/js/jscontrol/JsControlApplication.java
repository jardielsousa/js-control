package com.js.jscontrol;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JsControlApplication {

	public static void main(String[] args) {
		SpringApplication.run(JsControlApplication.class, args);
	}

}

